# librenms-plugin-SwitchList

A LibreNMS plugin to show a list of all switches and their respective model and software version, as well as location.

This plugin was made to fit our environment explicitly, so it might not fit your needs out of the box. 

_Made for Aurskog-Høland kommune._


# Install

Rename and copy this directory to librenms/html/plugins/SwitchList.

Edit the config.php file to fit reflect your environment.

In LibreNMS go to Overview->Plugins->Plugin Admin.

Click Enable on "SwitchList".


# Usage

Go to Overview->Plugins->Switch List.