<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default panel-condensed">
        <div class="panel-heading">
          <strong>Switch List</strong>
        </div>
        <table id="switch-list" class="table table-hover table-condensed table-striped">
          <thead>
            <tr>
              <th data-column-id="device_name" data-formatter="link">Device Name</th>
              <th data-column-id="device_hw">Hardware</th>
              <th data-column-id="device_os">OS</th>
              <th data-column-id="device_sw">Version</th>
              <th data-column-id="device_down">Down</th>
              <th data-column-id="device_loc" data-formatter="link">Location</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>
<script>
  $("#switch-list").bootgrid({
    rowCount: [-1,250,100,50,25],
    columnSelection: true,
    caseSensitive: false,
    multiSort: true,
    ajax: true,
    url: "/plugins/SwitchList/ajax.php",
    formatters: {
      "link": function(column,row) {
        if (typeof row[column.id] === 'object' && row[column.id]['text'] != undefined) {
          let output = row[column.id]['text']
          if (row[column.id]['url'] != undefined) {
            let cssclass = ''
            if (column.id == "device_name") { cssclass = 'list-device' }
            output = '<a href="' + row[column.id]['url'] + '" class="' + cssclass + '">' + row[column.id]['text'] + '</a>'
          }
          if (row[column.id]['status'] == "down") { 
            output += '<i class="fa fa-flag fa-lg" style="color:red" aria-hidden="true" title="Remote Port is down"></i>'
          }
          return output
        }
        return row[column.id]
      }
    }
  });
</script>