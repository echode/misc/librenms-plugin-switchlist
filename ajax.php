<?php

require 'config.php';
require '../../../includes/init.php';

if (!isset($_SERVER["HTTP_HOST"])) {
  parse_str($argv[1], $_POST);
}

$page = (int)$_POST['current'];
$max = (int)$_POST['rowCount'];
$start = 0;

$sql = "SELECT d.device_id,d.sysName,d.hardware,d.os,d.version,d.status_reason,l.location,l.id AS location_id FROM devices d left join locations l on d.location_id = l.id WHERE d.os IN ('".join("','",$os)."') ORDER BY hardware,version,sysName ASC";

$objects = array();
foreach (dbFetchRows($sql) as $row) {
  $object = array();
  $object['device_name'] = array();
  $object['device_name']['text'] = $row['sysName'];
  $object['device_name']['url'] = '/device/'.$row['device_id'];
  $object['device_hw'] = $row['hardware'];
  $object['device_os'] = $row['os'];
  $object['device_sw'] = $row['version'];
  $object['device_down'] = $row['status_reason'];
  $object['device_loc'] = array();
  $object['device_loc']['text'] = $row['location'];
  $object['device_loc']['url'] = '/devices/location='.$row['location_id'];
  $objects[] = $object;
}

function getstring($value,$param='text') {
  if (is_array($value)) {
    return strtolower($value[$param]);
  }
  return strtolower($value);
}

function sortobjects($sort) {
  if (!is_array($_POST['sort'])) { return $sort; }
  $keys = array_keys($_POST['sort']);
  foreach (array_reverse($keys) as $key) {
    usort($sort, function($a, $b) use ($key) {
      $stringa = getstring($a[$key]);
      $stringb = getstring($b[$key]);
      if ($_POST['sort'][$key] == 'desc') {
        return strcmp($stringb, $stringa);
      }
      return strcmp($stringa, $stringb);
    });
  }
  return $sort;
}

function filtervalues($value) {
  $search = strtolower($_POST['searchPhrase']);
  $string = getstring($value);
  return str_contains($string, $search);
}

function filterobjects($object) {
  $matches = array_filter($object, "filtervalues");
  $count = count($matches);
  if ($count > 0) { return true; }
  return false;
}

$total = count($objects);
if ($max < 1) { $max = $total; }
else { $start = ($page-1)*$max; }

$filtered = array_filter($objects, "filterobjects");
$sorted = array_values(sortobjects($filtered));
$paginated = array_slice($sorted, $start, $max);

$output = array();
$output['current'] = $page;
$output['total'] = count($filtered);
$output['rowCount'] = count($paginated);
$output['rows'] = array_values($paginated);

print json_encode($output);

?>
